package grmon

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var grid uint32 = 10000

type TGRMon struct {
	grmap *sync.Map
}

var grmon = &TGRMon{
	grmap: &sync.Map{},
}

func CatchPanic() {
	if err := recover(); err != nil {
		Log.Errorf("panic !!! err = %v ", err)
	}
}

func GetGRMon() *TGRMon {
	return grmon
}

func getGRName(name string) string {
	id := atomic.AddUint32(&grid, 1)
	return fmt.Sprintf("%v-%v", name, id)
}

func addGR(name string) {
	grmon.grmap.Store(name, 1)
}

func removeGR(name string) {
	grmon.grmap.Delete(name)
}

func Go(name string, fn interface{}, args ...interface{}) {

	go func() {

		defer CatchPanic()

		gname := getGRName(name)
		addGR(gname)
		defer removeGR(gname)

		if len(args) == 0 {
			f := fn.(func())
			f()
		} else {
			f := fn.(func(args ...interface{}))
			f(args...)
		}
	}()
}

func GoLoop(name string, fn interface{}, args ...interface{}) {

	go func() {

		defer CatchPanic()

		gname := getGRName(name)
		addGR(gname)
		defer removeGR(gname)

		for {
			if len(args) == 0 {
				f := fn.(func())
				f()
			} else {
				f := fn.(func(args ...interface{}))
				f(args...)
			}
		}
	}()
}
