package model

import (
	"time"

	"github.com/shopspring/decimal"
)

type User struct {
	Amount   decimal.Decimal
	Count    int
	CreateAt time.Time
	ID       int64
	Name     string
}

func (model User) TableName() string {
	return "user"
}
