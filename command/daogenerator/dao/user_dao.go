package dao

import (
	"errors"

	"bitbucket.org/shadow996/shadow/command/daogenerator/model"
	"bitbucket.org/shadow996/shadow/orm/jinzhu/gorm"
)

type UserDao struct {
	db *gorm.DB
}

func NewUserDao(db *gorm.DB) *UserDao {
	return &UserDao{
		db: db,
	}
}

func (dao *UserDao) Create(m *model.User) error {
	return dao.db.Create(m).Error
}

func (dao *UserDao) Find(m *model.User) (result []model.User, err error) {
	err = dao.db.Find(&result, m).Error
	return
}

func (dao *UserDao) FindOne(m *model.User) error {
	return dao.db.First(m, m).Error
}

func (dao *UserDao) FindPage(m *model.User, rowbound model.RowBound) (result []model.User, count int, err error) {
	err = dao.db.Count(&count).Limit(rowbound.Limit).Offset(rowbound.Offset).Find(&result, m).Error
	return
}

func (dao *UserDao) Get(m *model.User) error {
	if dao.db.NewRecord(m) {
		return errors.New("id is nil")
	}
	return dao.db.Find(m).Error
}

func (dao *UserDao) BatchGet(idbatch []int64) (result []model.User, err error) {
	err = dao.db.Model(&model.User{}).Where("ID in (?)", idbatch).Find(&model.User{}).Error
	return
}

func (dao *UserDao) GetForUpdate(m *model.User) error {
	if dao.db.NewRecord(m) {
		return errors.New("id is nil")
	}
	return dao.db.Set("gorm:query_option", "FOR UPDATE").Find(m).Error
}

func (dao *UserDao) Save(m *model.User) error {
	return dao.db.Save(m).Error
}

func (dao *UserDao) Delete(m *model.User) error {
	if dao.db.NewRecord(m) {
		return errors.New("id is nil")
	}
	return dao.db.Delete(m).Error
}

func (dao *UserDao) BatchDelete(idbatch []int64) error {
	return dao.db.Where("ID in (?)", idbatch).Delete(&model.User{}).Error
}

func (dao *UserDao) Updates(id int64, attrs map[string]interface{}) error {
	return dao.db.Model(&model.User{}).Where("ID = ?", id).Updates(attrs).Error
}

func (dao *UserDao) Update(id int64, attr string, value string) error {
	return dao.db.Model(&model.User{}).Where("ID = ?", id).Update(attr, value).Error
}

func (dao *UserDao) UpdateChanged(m *model.User) error {
	if dao.db.NewRecord(m) {
		return errors.New("id is nil")
	}

	var o = &model.User{
		ID: m.ID,
	}
	dao.Get(o)

	attrs := map[string]interface{}{}

	if !m.Amount.IsZero() {
		attrs["amount"] = m.Amount
	}

	if m.Count != o.Count {
		attrs["count"] = m.Count
	}

	if m.CreateAt != nil {
		attrs["create_at"] = m.CreateAt
	}

	if m.ID != o.ID {
		attrs["id"] = m.ID
	}

	if m.Name != o.Name {
		attrs["name"] = m.Name
	}

	return dao.db.Model(&model.User{}).Where("ID = ?", m.ID).Updates(attrs).Error
}

func (dao *UserDao) BatchUpdaterAttrs(idbatch []int64, attrs map[string]interface{}) error {
	return dao.db.Model(&model.User{}).Where("ID in (?)", idbatch).Updates(attrs).Error
}

func (dao *UserDao) Found(m *model.User) bool {
	find := dao.db.First(m, m).RecordNotFound()
	return !find
}
