module bitbucket.org/shadow996/shadow

go 1.15

require (
	github.com/casbin/casbin v1.9.1
	github.com/eahydra/gouuid v0.0.0-20130615145914-0bb88536d776
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/json-iterator/go v1.1.10
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6
	github.com/sirupsen/logrus v1.7.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
