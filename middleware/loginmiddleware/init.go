package loginmiddleware

import (
	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/middleware"
)

var (
	Log *logger.Logger
)

const (
	LOGIN_HANDLER = "LoginHandler"
)

func init() {
	Log = logger.InitLog()
	Log.Info("LoginHandler init")
	middleware.RegisterMiddlewareHandler(LOGIN_HANDLER, newDefaultLoginHandler)
}
