package securitymiddleware

import (
	"bitbucket.org/shadow996/shadow/logger"
)

var (
	Log *logger.Logger
)

func init() {
	Log = logger.InitLog()
	Log.Info("securitymiddleware middleware init")

}
