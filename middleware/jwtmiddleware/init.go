package jwtmiddleware

import (
	"bitbucket.org/shadow996/shadow/logger"
)

var (
	Log *logger.Logger
)

func init() {
	Log = logger.InitLog()
	Log.Info("JwtParser init")
}
