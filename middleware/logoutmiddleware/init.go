package logoutmiddleware

import (
	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/middleware"
	"github.com/astaxie/beego/session"
)

var (
	Log            *logger.Logger
	globalSessions *session.Manager
)

const (
	LOGOUT         = "logout"
	LOGOUT_HANDLER = "LogoutHandler"
)

func init() {
	Log = logger.InitLog()
	Log.Info("DefaultLogoutUrlRegistry init")
	middleware.RegisterMiddlewareHandler(LOGOUT_HANDLER, newDefaultLogoutHandler)
}
