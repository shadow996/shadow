package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServerConfig(t *testing.T) {
	serverConfigure := &TServerConfigure{}
	LoadWithFile(serverConfigure, "../config/server.json")
	assert.NotEmpty(t, serverConfigure.DataSource[0].IdlePoolSize)
	assert.NotEmpty(t, serverConfigure.DataSource[0].Password)
	assert.NotEmpty(t, serverConfigure.DataSource[0].MaxLifeTime)
	assert.NotEmpty(t, serverConfigure.DataSource[0].MaxPoolSize)
	assert.NotEmpty(t, serverConfigure.DataSource[0].Username)
	assert.NotEmpty(t, serverConfigure.DataSource[0].URL)
	assert.NotEmpty(t, serverConfigure.DataSource[0].SqlDebug)
	assert.NotEmpty(t, serverConfigure.Platform)
	assert.NotEmpty(t, serverConfigure.DataSource[0].SqlDebug)
	assert.NotEmpty(t, serverConfigure.Node)
}
