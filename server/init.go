package server

import (
	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/orm/datasource"
)

var Log *logger.Logger

const (
	DATASOURCE_CONFIGURE = "DataSourceConfigure"
)

func init() {
	Log = logger.InitLog()
	Log.Infoln("ServerConfigure init")

	datasource.RegisterDataSourceConfigure(DATASOURCE_CONFIGURE, newDataSourceConfigure)
}
