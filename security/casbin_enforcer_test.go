package security

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/shadow996/shadow/orm/datasource"
	_ "bitbucket.org/shadow996/shadow/orm/datasource/datasourcemanager"
	_ "bitbucket.org/shadow996/shadow/orm/jinzhu/gorm/dialects/mysql"
	"bitbucket.org/shadow996/shadow/server"
)

func TestCasbinFunc(t *testing.T) {
	datasource.RegisterDataSourceConfigure(server.DATASOURCE_CONFIGURE, newServerConfigure)
	enforcer := GetCasbinEnforcer()
	roles := enforcer.GetAllRoles()
	assert.NotEmpty(t, roles)
	for _, role := range roles {
		Log.Debug(role)
	}
}

func TestCasbinCheck(t *testing.T) {
	datasource.RegisterDataSourceConfigure(server.DATASOURCE_CONFIGURE, newServerConfigure)
	enforcer := GetCasbinEnforcer()
	result := enforcer.Enforce("test", "/", "/bcc/login", "POST")
	role := enforcer.GetRolesForUser("test")
	Log.Debug(role)
	assert.True(t, result)
}

func newServerConfigure() []datasource.IDataSourceConfigure {
	serverConfigure := &server.TServerConfigure{}
	server.LoadWithFile(serverConfigure, "../config/server.json")
	return []datasource.IDataSourceConfigure{&serverConfigure.DataSource[0]}
}
