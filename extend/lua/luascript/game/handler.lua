local utils = require("luautils")
local json = require("json")
local rpc = require("rpc")
local E = require("errmsg")
local slot = require("slot")
local time = require("time")
require("helper")



-- 查询用户信息
function queryUserInfoProcess(user, lreq, msg)
	return MakeSuccessResp(req.Header, user)
end

-- 查询用户钱包
function queryUserFundProcess(user, lreq, msg)
	local mercode = lreq.Header.MerchantCode
	local username = lreq.Header.UserName

	local resdata, err = getUserFundByName(mercode, username)
	if (err ~= nil) then
		return MakeErrResp(lreq.Header, MakeErr(E.EC_LUA_RPC_ERROR, err))
	end

	return MakeSuccessResp(lreq.Header, resdata)
end

-- 老虎机下单
function slotBetProcess(user, lreq, msg)
	local mercode = lreq.Header.MerchantCode
	local username = lreq.Header.UserName

	local subtype = lreq.Data.GameSubtype
	-- local multiple = lreq.Data.multiple
	-- local unit = lreq.Data.unit
	-- local linenum = lreq.Data.LineNum

	local order, free, game, ec = makeSlotBetOrder(req, user)
	if ec ~= nil then
		return MakeErrResp(lreq.Header, ec)
	end

	local linenum = order.LineNum
	local isBonus = order.IsFree

	local spinret = slot.Spin(subtype, linenum, isBonus)

	local codes = ""
	local stoplen = table.getn(spinret.Stop)
	for i, v in pairs(spinret.Stop) do
		if i < stoplen then
			codes = codes .. string.format("%d", v) .. "|"
		else
			codes = codes .. string.format("%d", v)
		end
	end

	order.AwardTime = time.Now()
	order.Codes = codes
	order.Ip = lreq.Header.ClientIp
	order.Location = utils.iploc(order.Ip)

	if spinret.IsWin then 
		order.Award = spinret.Award * multiple -- * unit
		order.FreeNum = spinret.FreeSpin.Prize
	end

	local err = doSlotBet(user, game, order, spinret, free)
	if err ~= nil then
		return MakeErrResp(lreq.Header, MakeErr(E.EC_LUA_RPC_ERROR, err))
	end 

	return MakeSuccessResp(lreq.Header, spinret)
end

function queryUserSlotStatisticProcess(user, lreq, msg)
	local mercode = lreq.Header.MerchantCode
	local username = lreq.Header.UserName

	local subtype = lreq.Data.GameSubtype

	local free = getLastFreeGame(mercode, username, subtype)

	return MakeSuccessResp(lreq.Header, free)
end

function querySlotBetProcess(user, lreq, msg)
	local mercode = lreq.Header.MerchantCode
	local username = lreq.Header.UserName
	local starttime = lreq.Data.StartTime
	local endtime = lreq.Data.EndTime

	local subtype = lreq.Data.GameSubtype


	return MakeSuccessResp(lreq.Header, free)
end

