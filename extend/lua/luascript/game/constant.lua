CONST = {
  UNIT_YUAN = "yuan",
  UNIT_JIAO = "jiao",
  UNIT_FEN = "fen",
  UNIT_LI = "li"
}

return CONST
