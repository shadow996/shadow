local utils = require("luautils")
-- local C = require("constant")
local E = require("errmsg")
local M = require("handler")
local json = require ("json")
local log = require ("lualog")

METHOD = {
	METHOD_QUERY_USER_INFO = "query_user_info",
	METHOD_QUERY_USER_FUND = "query_user_fund",
	METHOD_SLOT_BET = "slot_bet",
	METHOD_QUERY_USER_SLOT_STATISTIC = "query_user_slot_statistic",
	METHOD_QUERY_SLOT_BET_ORDER      = "query_slot_bet_order"
}

-- 函数处理表
proctable = {}
proctable[METHOD.METHOD_QUERY_USER_INFO] = queryUserInfoProcess
proctable[METHOD.METHOD_QUERY_USER_FUND] = queryUserFundProcess
proctable[METHOD.METHOD_SLOT_BET] = slotBetProcess
proctable[METHOD.METHOD_QUERY_USER_SLOT_STATISTIC] = queryUserSlotStatisticProcess



function GameProcess(user, req, msg)

  local method = req.Header.Method
  proc = proctable[method]
  if (proc == nil) then
    return MakeErrResp(req.Header, MakeErr(E.EC_LUA_INVALID_METHOD_ERROR, method))
  end

  local lreq, err = json.decode(msg)
	if (err ~= nil) then 
		return MakeErrResp(req.Header, MakeErr(E.EC_LUA_PARSE_JSON_ERROR, err))
	end

  local resp = proc(user, lreq, msg)

  return resp
end
