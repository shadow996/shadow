local C = require("constant")
local rpc = require("rpc")
local json = require("json")
local id = require("idgen")
local time = require("time")

function getMerColl(mercode, table)
  return mercode .. "_" .. table
end

function rpcGetVal(param)
  local res, err = rpc.GetValByHKey(param)
  if (err ~= nil) then
    return nil
  end

  local resdata, err = json.decode(res)
  if (err ~= nil) then
    return nil
  end

  return resdata
end

function rpcCall(param)
  local res, err = rpc.RpcCall(param)
  if (err ~= nil) then
    return nil
  end

  local resdata, err = json.decode(res)
  if (err ~= nil) then
    return nil
  end

  return resdata
end

function getUserFundByName(mercode, username)
  param = {
    Coll = getMerColl(mercode, "t_user_fund"),
    HKey = username,
    Db = mercode,
    Sql = "select * from t_user_fund where user_name=?",
    SqlParams = {username},
    StructType = "TUserFund"
  }

  return rpcGetVal(param)
end

function getSlotGameBySubtype(mercode, subtype)
  param = {
    -- Coll = getMerColl(mercode, "t_merchant_game_list"),
    -- HKey = subtype,
    Db = mercode,
    Sql = "select * from t_merchant_game_list where game_type='slot' and game_subtype=?",
    SqlParams = {subtype},
    StructType = "TMerchantGameList"
  }

  return rpcGetVal(param)
end

function getLastFreeGame(mercode, username, subtype)
  param = {
    -- Coll = getMerColl(mercode, "t_user_free_game"),
    -- HKey = subtype,
    Db = mercode,
    Sql = "select * from t_user_free_game where user_name=? and game_subtype=? and free_left > 0 order by id desc limit 1",
    SqlParams = {username, subtype},
    StructType = "TUserFreeGame"
  }

  return rpcGetVal(param)
end

function calcRealCost(betcount, multiple, unit)
  local perbet = 1
  local cost = perbet * betcount
  local realcost = 0

  if unit == UNIT_YUAN then
    realcost = cost * 1
  elseif unit == UNIT_JIAO then
    realcost = cost * 0.1
  elseif unit == UNIT_FEN then
    realcost = cost * 0.01
  elseif unit == UNIT_LI then
    realcost = cost * 0.001
  else -- 不会走这里
    assert(false)
  end

  realcost = realcost * multiple

  return realcost
end

function calcUnitAndMultiple(cost, multiple, unit)
  local perbet = 1
  local cost = perbet * betcount
  local realcost = 0

  if unit == UNIT_YUAN then
    realcost = cost * 1
  elseif unit == UNIT_JIAO then
    realcost = cost * 0.1
  elseif unit == UNIT_FEN then
    realcost = cost * 0.01
  elseif unit == UNIT_LI then
    realcost = cost * 0.001
  else -- 不会走这里
    assert(false)
  end

  realcost = realcost * multiple

  return realcost
end

function makeSlotBetOrder(lreq, user)
  local mercode = lreq.Header.TMerchantCode
  local username = lreq.Header.UserName
  local subtype = lreq.Data.GameSubtype
  local multiple = lreq.Data.multiple
  local unit = lreq.Data.unit
  local linenum = lreq.Data.LineNum

  if (multiple < 0) then
    return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "倍数错误")
  end

  if (unit ~= C.UNIT_YUAN and unit ~= C.UNIT_JIAO) then
    return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "单位错误")
  end

  local game = getSlotGameBySubtype(mercode, subtype)
  if game == nil then
    return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "无效游戏类型")
  end

  if linenum <= 0 or linenum > game.MaxLineNum then
    return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "无效选择线")
  end

  local freeleft = 0
  local free = getLastFreeGame(mercode, username, subtype)
  if free ~= nil then
    freeleft = free.FreeLeft
  end

  local order = {}
  order.OrderId = id.GenOrderId()
  order.UserId = user.UserId
  order.UserName = user.UserName
  order.IsTester = user.IsTester
  order.GameSubtype = subtype
  order.GameName = game.GameName
  order.Multiple = multiple
  order.Unit = unit
  order.LineNum = linenum
  order.OrderTime = time.Now()

  if freeleft > 0 then -- 有免费游戏
    order.IsFree = true
    order.RealCost = 0

    -- 由于是免费游戏, 忽略用户提交的参数使用免费游戏参数
    order.Multiple = free.FreeMultiple
    order.Unit = free.FreeUnit
    order.LineNum = free.FreeLineNum
  else -- 没有免费游戏
    local realcost = calcRealCost(1, order.Multiple, order.Unit)
    order.IsFree = false
    order.RealCost = realcost

    local userfund = getUserFundByName(mercode, username)
    if userfund == nil then
      return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "无效用户钱包")
    end

    if userfund.Balance < realcost then
      return nil, nil, nil, MakeErr(E.EC_LUA_PARAM_ERROR, "用户余额不足")
    end
  end

  return order, free, game, nil
end

function doSlotBet(user, game, order, spinret, free)
  param = {
    Params = {user, game, order, spinret, free}
  }

  return rpcCall("TRpcSvr.DoSlotBet", param)
end

function querySlotBetOrders(user, data)
  param = {
    Params = {user, data}
  }

  return rpcCall("TRpcSvr.QuerySlotBetOrders", param)
end
