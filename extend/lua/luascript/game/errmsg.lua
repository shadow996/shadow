
ERRCODE = {
  EC_SUCCESS = "0",
  EC_LUA_ERROR = "L8000",
  EC_LUA_INVALID_METHOD_ERROR = "L8001",
  EC_LUA_PARSE_JSON_ERROR = "L8002",
	EC_LUA_RPC_ERROR = "L8003",
	EC_LUA_PARAM_ERROR = "L8004"
}


ERRMSG = {}
ERRMSG[ERRCODE.EC_SUCCESS] = "成功"
ERRMSG[ERRCODE.EC_LUA_ERROR] = "服务器脚本错误 : %s"
ERRMSG[ERRCODE.EC_LUA_INVALID_METHOD_ERROR] = "无效请求 : %s"
ERRMSG[ERRCODE.EC_LUA_PARSE_JSON_ERROR] = "解析失败 : %s"
ERRMSG[ERRCODE.EC_LUA_RPC_ERROR] = "RPC调用失败 : %s"
ERRMSG[ERRCODE.EC_LUA_PARAM_ERROR] = "参数错误 : %s"

function MakeErr(errcode, ...)  
  ec = {
    Code = errcode,
    Desc = string.format(ERRMSG[errcode], unpack(arg))
  }

  return ec
end

function MakeRespHeader(reqheader, e)
	header = {
		MsgKey = reqheader.MsgKey,
		MsgType = reqheader.MsgType,
		Method = reqheader.Method,
		UserName = reqheader.UserName,
		Device = reqheader.Device,
		MerchantCode = reqheader.MerchantCode,
		NodeId = reqheader.NodeId,
		-- Path = reqheader.Path,
		ErrCode = e
	}

	return header
end

function MakeResp(reqheader, e, data)
	header = MakeRespHeader(reqheader, e)

	resp = {
		Header = header,
		Data = data
	}

	return resp
end

function MakeErrResp(reqheader, e)
	return MakeResp(reqheader, e, nil)
end

function MakeErrResp2(reqheader, e, data)
	return MakeResp(reqheader, e, data)
end

function MakeSuccessResp(reqheader, data)
	return MakeResp(reqheader, MakeErr(ERRCODE.EC_SUCCESS), data)
end

function MakeSuccessResp2(reqheader)
	return MakeResp(reqheader, MakeErr(ERRCODE.EC_SUCCESS), nil)
end

return ERRCODE