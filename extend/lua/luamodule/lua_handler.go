package gameserver

// import (
// 	. "comutils/serverutils"

// 	// . "swiftserver/datahelper"

// 	// . "swiftserver/mqcli"
// 	. "swiftserver/protocol"
// 	// . "swiftserver/rpcserver/rpcprotocol"
// 	// . "swiftserver/servernode"
// 	// . "swiftserver/gameserver/common"
// 	. "swiftserver/luamodule"

// 	luamapper "github.com/yuin/gluamapper"
// 	lua "github.com/yuin/gopher-lua"
// 	luar "layeh.com/gopher-luar"
// )

// func LuaGameProcess(user *TUser, req *TMsgReq, msg string) string {

// 	L := lua.NewState()
// 	defer L.Close()

// 	LuaLoadModules(L)

// 	// to do - 优化为缓存加载
// 	err := L.DoFile("./luascript/game/gameprocess.lua")
// 	// err := L.DoString(`print("aaa")`)
// 	if err != nil {
// 		Log.Error("err = %v", err)
// 		return MakeErrResp(&req.Header, MakeErr(EC_SERVER_INTERNAL_ERROR, err))
// 	}

// 	vf := L.GetGlobal("GameProcess")
// 	err = L.CallByParam(
// 		lua.P{
// 			Fn:      vf,
// 			NRet:    1,
// 			Protect: true,
// 		},
// 		luar.New(L, user),
// 		luar.New(L, req),
// 		lua.LString(msg),
// 	)
// 	if err != nil {
// 		Log.Error("call err = %v", err)
// 		return MakeErrResp(&req.Header, MakeErr(EC_SERVER_INTERNAL_ERROR, err))
// 	}

// 	ret := L.Get(-1) // returned value
// 	L.Pop(1)         // remove received value

// 	var resp TMsgResp2
// 	err = luamapper.Map(ret.(*lua.LTable), &resp)
// 	if err != nil {
// 		Log.Error("parse resp failed, err = %v", err)
// 		return MakeErrResp(&req.Header, MakeErr(EC_SERVER_INTERNAL_ERROR, err))
// 	}

// 	return SerializeToJson(resp)
// }
