package luamodule

import (
	. "comutils/serverutils"
	// 
	// . "swiftserver/datahelper"
	// 
	// . "swiftserver/mqcli"
	// . "swiftserver/protocol"
	// . "swiftserver/rpcserver/rpcprotocol"
	// . "swiftserver/servernode"
	// . "swiftserver/gameserver/common"

	"swiftserver/luamodule/idgen"
	"swiftserver/luamodule/json"
	"swiftserver/luamodule/lualog"
	"swiftserver/luamodule/luautils"
	"swiftserver/luamodule/slot"
	"swiftserver/luamodule/time"

	lua "github.com/yuin/gopher-lua"
	// "log"
	// "net/http"
	// 
	// 
	// . "swiftserver/mqcli"
	// . "swiftserver/rediscli"
	// . "swiftserver/rpcserver/rpcprotocol"
	// . "swiftserver/servernode"
	// "time"
	// "version"
)

type TLuaEntry struct {
	Path      string
	EntryFunc string
}

func LuaInit() {
	lua.RegistrySize = 1024 * 20
	lua.CallStackSize = 1024
	lua.LuaPathDefault = "./luascript/dispatch/?.lua;./luascript/game/?.lua"
	Log.Debug("regsize = %v, callstack size=%v, LuaPathDefault = %v",
		lua.RegistrySize, lua.CallStackSize, lua.LuaPathDefault)
}

func LuaLoadModules(L *lua.LState) {
	L.PreloadModule("luautils", luautils.Loader)
	L.PreloadModule("json", json.Loader)
	// L.PreloadModule("rpc", rpc.Loader)
	L.PreloadModule("lualog", lualog.Loader)
	L.PreloadModule("idgen", idgen.Loader)
	L.PreloadModule("time", time.Loader)
	L.PreloadModule("slot", slot.Loader)
}
