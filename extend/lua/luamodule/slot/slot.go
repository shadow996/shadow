package slot

import (
	// . "comutils/serverutils"
	//
	// . "swiftserver/rpcserver/rpcprotocol"

	slot "gameimplement/slot/main"

	"github.com/yuin/gopher-lua"
	luar "layeh.com/gopher-luar"
)

// Preload adds json to the given Lua state's package.preload table. After it
// has been preloaded, it can be loaded using require:
//
func Preload(L *lua.LState) {
	L.PreloadModule("slot", Loader)
}

// Loader is the module loader function.
func Loader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, api)
	L.Push(t)
	return 1
}

var api = map[string]lua.LGFunction{
	"Spin": Spin,
}

func Spin(L *lua.LState) int {
	subtype := L.CheckString(1)
	linenum := L.CheckInt(2)
	isBonus := L.CheckBool(3)

	spinret := slot.Spin(subtype, linenum, isBonus)

	L.Push(luar.New(L, spinret))
	return 1
}
