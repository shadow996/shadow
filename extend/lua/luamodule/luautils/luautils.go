package luautils

import (
	. "comutils/serverutils"

	luamapper "github.com/yuin/gluamapper"
	"github.com/yuin/gopher-lua"
)

// Preload adds json to the given Lua state's package.preload table. After it
// has been preloaded, it can be loaded using require:
//
func Preload(L *lua.LState) {
	L.PreloadModule("luautils", Loader)
}

// Loader is the module loader function.
func Loader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, api)
	L.Push(t)
	return 1
}

var api = map[string]lua.LGFunction{
	"printtable": printtable,
	"iploc":      iploc,
}

func printtable(L *lua.LState) int {
	lv := L.Get(-1)

	var v interface{}
	luamapper.Map(lv.(*lua.LTable), &v)

	Log.Info("%+v", (v))
	return 0
}

func iploc(L *lua.LState) int {
	ip := L.CheckString(1)

	iploc := IpLocStr(ip)

	L.Push(lua.LString(iploc))
	return 1
}
