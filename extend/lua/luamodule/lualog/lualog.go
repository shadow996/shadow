package lualog

import (
	. "comutils/serverutils"

	luamapper "github.com/yuin/gluamapper"

	lua "github.com/yuin/gopher-lua"
)

// Preload adds json to the given Lua state's package.preload table. After it
// has been preloaded, it can be loaded using require:
//
func Preload(L *lua.LState) {
	L.PreloadModule("lualog", Loader)
}

// Loader is the module loader function.
func Loader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, api)
	L.Push(t)
	return 1
}

var api = map[string]lua.LGFunction{
	"Error":   Error,
	"Debug":   Debug,
	"Warning": Warning,
	"Info":    Info,
}

const (
	L_ERROR   = "ERROR"
	L_DEBUG   = "DEBUG"
	L_WARNING = "WARNING"
	L_INFO    = "INFO"
)

func logPrint(L *lua.LState, level string) int {
	num := L.GetTop()
	var param []interface{}

	format := "[LUA] " + L.CheckString(1)

	for i := 2; i <= num; i++ {
		val := L.CheckAny(i)
		vtype := val.Type()
		if vtype == lua.LTUserData {
			param = append(param, val.(*lua.LUserData).Value)
		} else if vtype == lua.LTTable {
			vmap := make(map[string]interface{})
			luamapper.Map(val.(*lua.LTable), &vmap)
			param = append(param, vmap)
		} else {
			param = append(param, val)
		}
	}

	if level == L_ERROR {
		Log.Error(format, param...)
	} else if level == L_DEBUG {
		Log.Debug(format, param...)
	} else if level == L_WARNING {
		Log.Warning(format, param...)
	} else {
		Log.Info(format, param...)
	}

	return 0
}

func Error(L *lua.LState) int {
	return logPrint(L, L_ERROR)
}

func Debug(L *lua.LState) int {
	return logPrint(L, L_DEBUG)
}

func Warning(L *lua.LState) int {
	return logPrint(L, L_WARNING)
}

func Info(L *lua.LState) int {
	return logPrint(L, L_INFO)
}
