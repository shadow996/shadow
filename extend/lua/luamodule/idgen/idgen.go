package idgen

// import (
// 	"public/service"

// 	lua "github.com/yuin/gopher-lua"
// )

// // Preload adds json to the given Lua state's package.preload table. After it
// // has been preloaded, it can be loaded using require:
// //
// func Preload(L *lua.LState) {
// 	L.PreloadModule("idgen", Loader)
// }

// // Loader is the module loader function.
// func Loader(L *lua.LState) int {
// 	t := L.NewTable()
// 	L.SetFuncs(t, api)
// 	L.Push(t)
// 	return 1
// }

// var api = map[string]lua.LGFunction{
// 	"GenOrderId":      GenOrderId,
// 	"GenFundChangeId": GenFundChangeId,
// }

// func GenOrderId(L *lua.LState) int {
// 	orderid := service.GenOrderId()
// 	L.Push(lua.LString(orderid))
// 	return 1
// }

// func GenFundChangeId(L *lua.LState) int {
// 	fid := service.GenFundChangeId()
// 	L.Push(lua.LString(fid))
// 	return 1
// }
