package rpc

// import (
// 	. "comutils/serverutils"
// 	//
// 	. "swiftserver/rpcprotocol"

// 	luamapper "github.com/yuin/gluamapper"

// 	"github.com/yuin/gopher-lua"
// )

// // Preload adds json to the given Lua state's package.preload table. After it
// // has been preloaded, it can be loaded using require:
// //
// func Preload(L *lua.LState) {
// 	L.PreloadModule("rpc", Loader)
// }

// // Loader is the module loader function.
// func Loader(L *lua.LState) int {
// 	t := L.NewTable()
// 	L.SetFuncs(t, api)
// 	L.Push(t)
// 	return 1
// }

// var api = map[string]lua.LGFunction{
// 	"RpcCall":      RpcCall,
// 	"GetValByHKey": GetValByHKey,
// 	"SetValByHKey": SetValByHKey,
// }

// func RpcCall(L *lua.LState) int {
// 	smethod := L.CheckString(1)
// 	arg := L.CheckTable(2)

// 	var rpcparam TRpcParam
// 	err := luamapper.Map(arg, &rpcparam)
// 	if err != nil {
// 		Log.Error("parse rpc param failed, err = %v", err)
// 		L.Push(lua.LNil)
// 		L.Push(lua.LString(err.Error()))
// 		return 2
// 	}

// 	var reply string
// 	err = RpcCli.RpcCall(smethod, arg, &reply)
// 	if err != nil {
// 		Log.Error("rpc call failed, err = %v", err)
// 		L.Push(lua.LNil)
// 		L.Push(lua.LString(err.Error()))
// 		return 2
// 	}

// 	L.Push(lua.LString(reply))
// 	return 1
// }

// func GetValByHKey(L *lua.LState) int {
// 	tbl := L.CheckTable(1)

// 	var param TRpcParam
// 	err := luamapper.Map(tbl, &param)
// 	if err != nil {
// 		Log.Error("parse rpc param failed, err = %v", err)

// 		L.Push(lua.LNil)
// 		L.Push(lua.LString(err.Error()))
// 		return 2
// 	}

// 	result, err := RpcCli.GetValByHKey(param)
// 	if err != nil {
// 		Log.Warning("GetValByHKey failed, err = %v", err)
// 		L.Push(lua.LNil)
// 		L.Push(lua.LString(err.Error()))
// 		return 2
// 	}

// 	L.Push(lua.LString(result))
// 	return 1
// }

// func SetValByHKey(L *lua.LState) int {
// 	tbl := L.CheckTable(1)

// 	var param TRpcParam
// 	err := luamapper.Map(tbl, &param)
// 	if err != nil {
// 		Log.Error("parse rpc param failed, err = %v", err)

// 		L.Push(lua.LString(err.Error()))
// 		return 1
// 	}

// 	err = RpcCli.SetValByHKey(param)
// 	if err != nil {
// 		Log.Warning("SetValByHKey failed, err = %v", err)
// 		L.Push(lua.LString(err.Error()))
// 		return 1
// 	}

// 	return 0
// }
