package time

import (
	. "comutils/serverutils"
	//
	// . "swiftserver/rpcserver/rpcprotocol"

	"github.com/yuin/gopher-lua"
)

// Preload adds json to the given Lua state's package.preload table. After it
// has been preloaded, it can be loaded using require:
//
func Preload(L *lua.LState) {
	L.PreloadModule("time", Loader)
}

// Loader is the module loader function.
func Loader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, api)
	L.Push(t)
	return 1
}

var api = map[string]lua.LGFunction{
	"Now": Now,
}

func Now(L *lua.LState) int {

	nowstr := NiNowStr()

	L.Push(lua.LString(nowstr))
	return 1
}
