package httpserve

import "net/http"

func HttpServe(svr *http.Server) error {
	return http.ListenAndServe(svr.Addr, svr.Handler)
}
