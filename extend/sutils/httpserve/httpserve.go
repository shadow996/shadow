// +build !windows

package httpserve

import (
	"net/http"

	"bitbucket.org/shadow996/shadow/extend/sutils/facebookgo/grace/gracehttp"
)

func HttpServe(svr *http.Server) error {
	return gracehttp.Serve(svr)
}
