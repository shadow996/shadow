package captcha

import (
	"bytes"
	"errors"
	"image"
	"image/png"
	"os"
	"sync"

	"bitbucket.org/shadow996/shadow/extend/sutils"
	"bitbucket.org/shadow996/shadow/extend/sutils/captcha/gocaptcha"
	. "bitbucket.org/shadow996/shadow/logger"
)

var CaptchaCtrl TCaptchaCtrl

// code 之外的验证信息, 用于加强验证码验证
type TExtraVerifyItem struct {
	UserName  string
	ForMethod string
}

type TCaptchaCtrl struct {
	Captcha        *gocaptcha.Captcha
	ExtraVerifyMap map[string]TExtraVerifyItem
	Mutex          sync.Mutex
	Disabled       bool
}

func CaptchaInit(captchaConfig string) {

	// set captcha log
	captcha, err := gocaptcha.CreateCaptchaFromConfigFile(captchaConfig)
	if nil != err {
		Log.Errorf("config load failed:%s", err.Error())
		os.Exit(-7)
	} else {
		CaptchaCtrl.Captcha = captcha
		CaptchaCtrl.ExtraVerifyMap = make(map[string]TExtraVerifyItem)
		CaptchaCtrl.Disabled = false
	}
}

func CaptchaGetKeyAndImage(username string) (string, string, error) {

	CaptchaCtrl.Mutex.Lock()
	defer CaptchaCtrl.Mutex.Unlock()

	item := TExtraVerifyItem{UserName: username}
	key, err := CaptchaCtrl.Captcha.GetKey(4)
	if err != nil {
		return "", "", err
	}

	image, err := CaptchaCtrl.Captcha.GetImage(key)
	if err != nil {
		return "", "", err
	}

	buff := new(bytes.Buffer)
	err = png.Encode(buff, image)
	if err != nil {
		return "", "", err
	}

	imgb64 := sutils.Base64Encode(buff.Bytes())
	imgstr := "data:image/png;base64," + string(imgb64)

	CaptchaCtrl.ExtraVerifyMap[key] = item

	return key, imgstr, nil
}

func CaptchaGetImage(key string) (image.Image, error) {
	return CaptchaCtrl.Captcha.GetImage(key)
}

func CaptchaVerifyCode(username string, key string, code string) (bool, error) {

	if CaptchaCtrl.Disabled {
		return true, nil
	}

	CaptchaCtrl.Mutex.Lock()
	defer CaptchaCtrl.Mutex.Unlock()
	defer delete(CaptchaCtrl.ExtraVerifyMap, key) // 不管是否验证成功,清理验证码,保证其只能用一次

	_, exists := CaptchaCtrl.ExtraVerifyMap[key]
	if !exists {
		return false, errors.New("Invalid request for verification")
	}

	ok, msg := CaptchaCtrl.Captcha.Verify(key, code)
	if !ok {
		return false, errors.New(msg)
	}

	return true, nil
}
