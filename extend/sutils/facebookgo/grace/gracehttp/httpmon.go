package gracehttp

import (
	"net"
	"sync"
	"time"

	. "bitbucket.org/shadow996/shadow/logger"
)

// ===> added by matt 20170714 begin
// add customized listener for connection restriction

type TLimitListener struct {
	net.Listener
}

type TLimitConn struct {
	net.Conn
}

type TLimitConfig struct {
	MaxConnPerIp int
}

type TConnStatis struct {
	CurrentConnNum int
	MaxConnNum     int
	MaxConnTime    time.Time
	Mutex          sync.Mutex
}

var ConnStatis TConnStatis

func (s *TConnStatis) ConnNumInc() {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()
	s.CurrentConnNum++
	if s.MaxConnNum < s.CurrentConnNum {
		s.MaxConnNum++
		s.MaxConnTime = time.Now()
	}
}

func (s *TConnStatis) ConnNumDec() {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()
	s.CurrentConnNum--
}

func (s *TConnStatis) ConnNumVal() int {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()

	num := s.CurrentConnNum

	return num
}

func (s *TConnStatis) MaxConnNumVal() (int, time.Time) {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()

	num := s.MaxConnNum
	t := s.MaxConnTime

	return num, t
}

func (l *TLimitListener) Accept() (net.Conn, error) {

	conn, err := l.Listener.Accept()
	if err != nil {
		return conn, err
	}

	rip := conn.RemoteAddr()
	Log.Debugf("Listener[%v] TLimitListener Accept connection=[%v] from ip:[%v]", l, conn, rip)

	ConnStatis.ConnNumInc()
	var conn2 net.Conn = &TLimitConn{Conn: conn}

	return conn2, err
}

func (l *TLimitListener) Close() error {
	// Log.Info("Lisenter[%v] TLimitListener Close", l)
	return l.Listener.Close()
}

func (l *TLimitListener) Addr() net.Addr {
	// Log.Info("Lisenter[%v] TLimitListener Addr", l)
	return l.Listener.Addr()
}

// TLimitConn functions for interface
func (c *TLimitConn) Read(b []byte) (n int, err error) {
	// Log.Info("Conn[%v] TLimitConn Read", c)
	return c.Conn.Read(b)
}

func (c *TLimitConn) Write(b []byte) (n int, err error) {
	// Log.Info("Conn[%v] TLimitConn Write", c)
	return c.Conn.Write(b)
}

func (c *TLimitConn) Close() error {
	rip := c.RemoteAddr()
	Log.Debugf("Conn[%v] TLimitConn Close, remote ip = %v", c, rip)

	ConnStatis.ConnNumDec()

	return c.Conn.Close()
}

func (c *TLimitConn) LocalAddr() net.Addr {
	// Log.Info("Conn[%v] TLimitConn LocalAddr", c)
	return c.Conn.LocalAddr()
}

func (c *TLimitConn) RemoteAddr() net.Addr {
	// Log.Info("Conn[%v] TLimitConn RemoteAddr", c)
	return c.Conn.RemoteAddr()
}

func (c *TLimitConn) SetDeadline(t time.Time) error {
	// Log.Info("Conn[%v] TLimitConn SetDeadline, t = %v", c, t)
	return c.Conn.SetDeadline(t)
}

func (c *TLimitConn) SetReadDeadline(t time.Time) error {
	// Log.Info("Conn[%v] TLimitConn SetReadDeadline, t = %v", c, t)
	return c.Conn.SetReadDeadline(t)
}

func (c *TLimitConn) SetWriteDeadline(t time.Time) error {
	// Log.Info("Conn[%v] TLimitConn SetWriteDeadline, t = %v", c, t)
	return c.Conn.SetWriteDeadline(t)
}

// ===> added by matt 20170714 end
