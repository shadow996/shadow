package base

import (
	. "bitbucket.org/shadow996/shadow/extend/global"
	"bitbucket.org/shadow996/shadow/extend/sutils"
)

var goRoutineManager *sutils.GoRoutineManager = sutils.NewGoRoutineManager()

func GetGoRoutineManager() *sutils.GoRoutineManager {
	ASSERT(goRoutineManager != nil)
	return goRoutineManager
}
