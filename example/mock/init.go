package mock

import (
	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/security"
)

var (
	Log *logger.Logger
)

func init() {
	Log = logger.InitLog()
	security.RegisterUserDetailService(security.USER_DETAILS_SERVICE, newMyUserDetailsService)
}
