package mock

import "bitbucket.org/shadow996/shadow/security"

type (
	TMyUserDetailsService struct{}
)

func newMyUserDetailsService() interface{} {
	return new(TMyUserDetailsService)
}

func (myUserDetailsService TMyUserDetailsService) LoadUserByUsername(username string) security.IUserDetails {
	userDetails := new(security.TUser)
	userDetails.SetUsername("test")
	userDetails.SetPassword("$2a$10$b5qQ97pqSEv3511uQ7GEy.ClmoBrx5aQ7ugDiy3JrPbsphsFR5rye") // 123456
	userDetails.SetAccountExpired(false)
	userDetails.SetAccountLocked(false)
	userDetails.SetCredentialsExpired(false)
	return userDetails
}
