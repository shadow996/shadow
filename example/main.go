package main

import (
	"net/http"
	"time"

	"bitbucket.org/shadow996/shadow/middleware/concurrentlimit"

	_ "bitbucket.org/shadow996/shadow/example/mock"
	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/middleware/securitymiddleware"
	"bitbucket.org/shadow996/shadow/middleware/sessionmiddleware"
	_ "bitbucket.org/shadow996/shadow/orm/datasource/datasourcemanager"
	_ "bitbucket.org/shadow996/shadow/orm/jinzhu/gorm/dialects/mysql"
	"bitbucket.org/shadow996/shadow/server"
	_ "bitbucket.org/shadow996/shadow/validator"
	"github.com/astaxie/beego/session"
	_ "github.com/astaxie/beego/session/mysql"
	"github.com/gin-gonic/gin"
)

var (
	Log *logger.Logger
)

func init() {
	Log = logger.InitLog()
}

func main() {
	Log.Info("main run")

	e := DefaultEngine()

	e.GET("/", func(c *gin.Context) {
		// time.Sleep(1 * time.Second)
		c.JSON(http.StatusOK, gin.H{
			"message": "hello world",
		})
	})

	// e.GET("/myaccount", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"message": "myaccount",
	// 	})
	// })

	// e.GET("/login", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"message": "login",
	// 	})
	// })

	// e.GET("/test", func(c *gin.Context) {
	// 	c.HTML(200, "test.gohtml", gin.H{})
	// })

	// e.LoadHTMLGlob("./example/template/*")

	e.Run("127.0.0.1:8080")

}

// DefaultEngine will add all middleware for default
func DefaultEngine() *gin.Engine {
	engine := gin.New()
	engine.Use(concurrentlimit.ConcurrentLimit(2, 30*time.Second))
	engine.Use(gin.Logger())
	engine.Use(gin.Recovery())

	serverConfig := server.ServerConfigureInstance()
	server.LoadWithFile(serverConfig, server.DefaultConfigurePath)

	dataSource := serverConfig.GetDataSource()[0]
	dbstr := dataSource.GetURL()
	globalSessions, err := session.NewManager("mysql", &session.ManagerConfig{
		CookieName:      "gosessionid",
		Gclifetime:      3600,
		EnableSetCookie: true,
		ProviderConfig:  dbstr,
	})
	if err != nil {
		panic(err)
	}
	go globalSessions.GC()
	engine.Use(sessionmiddleware.Sessions(globalSessions))
	// engine.Use(sessionmiddleware.SessionRelease())

	// Set a hook for i18n, because i18n funcMap used in template must set before middleware pipeline run,
	// but we create funcMap accordding to the customer request during the middleware, so, here set a hook for return a funcmap when middleware run
	// var Hook i18n.TranslateFunc
	// engine.Use(i18nmiddleware.I18nResolver(&Hook))
	// engine.SetFuncMap(template.FuncMap{
	// 	"T": func(str string) string {
	// 		return Hook(str)
	// 	},
	// })

	engine.Use(securitymiddleware.SecurityTokenResolver())

	// engine.Use(logoutmiddleware.LogoutFilter(middleware.UrlRegistryInstance(middleware.LOGOUT).GetPath()))
	// engine.Use(securitymiddleware.UsernamePasswordLoginFilter(middleware.UrlRegistryInstance(middleware.LOGIN).GetPath()))
	engine.Use(securitymiddleware.AnonymousFilter())
	engine.Use(securitymiddleware.Authorizer())
	// engine.Use(errormiddleware.ErrorHandler())
	// engine.Use(gzip.Gzip(gzip.DefaultCompression))

	// // // Set default router
	// engine.Any(middleware.UrlRegistryInstance(middleware.LOGOUT).GetPath(), func(c *gin.Context) {
	// 	middleware.MiddlewareHandlerInstance(logoutmiddleware.LOGOUT_HANDLER).Handle(c)
	// })
	// engine.POST(middleware.UrlRegistryInstance(middleware.LOGIN).GetPath(), func(c *gin.Context) {
	// 	middleware.MiddlewareHandlerInstance(loginmiddleware.LOGIN_HANDLER).Handle(c)
	// })

	return engine
}
