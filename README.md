# shadow-framework

shaow-framework 是一个简单的 web 框架，集成了 github 上比较流行的一些 lib 包和其他框架，主要是集成了 gin，gorm，casbin，logrus，go-i18n 等框架

gin 是一个 web 框架，请参考 https://github.com/gin-gonic/gin
gorm 是一个 orm 框架，请参考 https://bitbucket.org/shadow996/shadow/orm/jinzhu/gorm
casbin 是一个权限验证框架，请参考 https://github.com/casbin/casbin
logrus 是一个 log 框架，请参考 https://github.com/sirupsen/logrus
go-i18n 是一个国际化框架，请参考 https://github.com/nicksnyder/go-i18n

### best practice：

1. 不用使用 git clone 命令来下载任何源代码, 所有需要用到 git clone 命令的地方，全部用 go get 代替
2. 使用 dep ensure -add 添加依赖包，不要用 git clone 和 go get 添加依赖包
3. 使用 dep ensure -update 更新依赖包，不要用 git clone 和 go get 更新依赖包

# quick start

## 如何使用 shadow-framework

1. shadow-framework 使用 dep 来管理包依赖, dep 安装参考https://github.com/golang/dep
2. 确保系统已经配置了 GO_PATH，并且将 GO_PATH/bin 加入 PATH
3. 在要引入 shadow-framework 的工程的根目录下执行 dep init，此时会在当前目录生成一个 vendor 目录和 Gopke.lock, Gopkg.toml 文件

```
    dep init
```

4.  继续在当前目录执行 go fetch 命令，加入 shadow-framework 的依赖

        dep ensure -add bitbucket.org/shadow996/shadow

    

5.  此时在 vendor 目录下应该已经加入了所有依赖, 现在可以直接在代码中引用 shadow

```go
import (
	"bitbucket.org/shadow996/shadow"
	"bitbucket.org/shadow996/shadow/security"
)

e := shadow.DefaultEngine()
e.GET("/", func(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{
        "message": "hello world",
    })
})
```

## 如何开发 shadow-framework

1. 如果没有 dep，请先安装 dep
2. 设置 GOPATH, 如果已经有 GOPATH，可以直接使用，或者要新建 GOPATH，请将最新的 GOPATH 置于其他 GOPATH 之前
3. 使用 env GIT_TERMINAL_PROMPT=1 go get bitbucket.org/shadow996/shadow 命令下载代码到 GOPATH
4. 进入$GOPATH/src/bitbucket.org/shadow996/shadow 目录，运行 dep ensure, dep 会自动添加所有的依赖到 vendor 目录
5. 完毕

# shadow-framework interface

shadow-framework 提供了很多扩展点可以使使用者方便的进行功能扩展

## 数据库配置

默认数据库配置是用户提供一个配置文件，默认的路径为项目根目录下的./orm/config/datasource.json

```json
{
  "username": "root",
  "password": "111111",
  "url": "root:111111@tcp(127.0.0.1:3306)/casbin?charset=utf8&parseTime=True&loc=Local",
  "driver": "mysql"
}
```

如果想改变配置文件路径或使用其他格式的配置文件可以自己实现一个 Configure 接口的实现，然后将此实现注册进我们的框架

```go
type Configure interface {
	Username() string
	Password() string
	Url() string
	Driver() string
}

```

```go
RegisterConfigure("Configure", yourConfigureConstructor)
```
