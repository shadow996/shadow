package datasourcemanager

import (
	"sync"

	"bitbucket.org/shadow996/shadow/logger"
	"bitbucket.org/shadow996/shadow/orm/datasource"
)

var (
	Log *logger.Logger
	l   sync.Mutex
)

func init() {
	Log = logger.InitLog()
	Log.Infoln("GormDataSourceManager init")
	datasource.RegisterDatasourceManager(datasource.DATASOURCE_MANAGER, newGormDataSourceManager)
	datasource.RegisterShardingDatasourceManager(datasource.DATASOURCE_MANAGER, newGormShardingDatasourceManager)
}
