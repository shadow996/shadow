package datasourcemanager

import (
	"testing"

	"bitbucket.org/shadow996/shadow/orm/datasource"
	_ "bitbucket.org/shadow996/shadow/orm/jinzhu/gorm/dialects/mysql"
	"bitbucket.org/shadow996/shadow/server"
)

func TestCreateDatabase(t *testing.T) {
	dataSourceManager := &TGormDataSourceManager{
		configs: []datasource.IDataSourceConfigure{newServerConfigure()},
	}
	dataSourceManager.tryToCreateDatabase()
}

func newServerConfigure() datasource.IDataSourceConfigure {
	serverConfigure := &server.TServerConfigure{}
	server.LoadWithFile(serverConfigure, "../../../config/server.json")
	return &serverConfigure.DataSource[0]
}
