package datasource

import (
	"bitbucket.org/shadow996/shadow/logger"
)

var Log *logger.Logger

const (
	DATASOURCE_MANAGER = "DataSourceManager"
)

func init() {
	Log = logger.InitLog()
}
