package model

import "time"

type TModel struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type TUsernamePassword struct {
	username string
	password string
}
