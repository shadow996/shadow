package idgenerator

import (
	"testing"

	"bitbucket.org/shadow996/shadow/orm/datasource"
	_ "bitbucket.org/shadow996/shadow/orm/datasource/datasourcemanager"
	_ "bitbucket.org/shadow996/shadow/orm/jinzhu/gorm/dialects/mysql"
	"bitbucket.org/shadow996/shadow/server"
	"github.com/stretchr/testify/assert"
)

func newServerConfigure() []datasource.IDataSourceConfigure {
	serverConfigure := &server.TServerConfigure{}
	server.LoadWithFile(serverConfigure, "../config/server.json")
	return []datasource.IDataSourceConfigure{&serverConfigure.DataSource[0]}
}

func TestGenerateStringID(t *testing.T) {
	datasource.RegisterDataSourceConfigure(server.DATASOURCE_CONFIGURE, newServerConfigure)
	Init()
	gen := Instance()
	assert.NotNil(t, gen)
	for i := 0; i < 2000; i++ {
		id := gen.GenerateStringID("merchant")
		Log.Println(id)
		assert.NotEmpty(t, id)
	}
}

func TestGenerateRandID(t *testing.T) {
	datasource.RegisterDataSourceConfigure(server.DATASOURCE_CONFIGURE, newServerConfigure)
	Init()
	gen := Instance()
	assert.NotNil(t, gen)
	Log.Println(gen.GenerateGuardID("order"))
}

func TestMessID(t *testing.T) {
	key := "0ZV6AWDFGSCU9HJL578X1MBKN24QERTYIOP3"

	datasource.RegisterDataSourceConfigure(server.DATASOURCE_CONFIGURE, newServerConfigure)
	Init()
	gen := Instance()
	assert.NotNil(t, gen)
	l := gen.GenerateLongID("order")
	Log.Printf("long : %d", l)

	s := gen.ChaosID(l, key)
	Log.Printf("string : %s", s)

	sl := gen.RestoreID(s, key)
	Log.Printf("long : %d", sl)
}
